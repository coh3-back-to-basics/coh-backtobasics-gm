# COH3 Back to Basics Game Mode

Game mode that's meant to be used with Back to Basics tuning pack.

Features:
- Advanced AI integration.
- New camera defaults.
- Extra global unit modifiers.
- New combat mechanics:
	- Enfilade.
	- Anti-blob.
	- Self-healing.
