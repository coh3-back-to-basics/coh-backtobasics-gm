--#region Debugging setup
if os.getenv("LOCAL_LUA_DEBUGGER_VSCODE") == "1" then
	local lldebugger = require("lldebugger")
	lldebugger.start()
	local run = love.run
	function love.run(...)
		local f = lldebugger.call(run, false, ...)
		return function(...) return lldebugger.call(f, false, ...) end
	end
end
--#endregion

local sqrt = math.sqrt
local floor = math.floor
local ceil = math.ceil
local abs = math.abs
local min = math.min
local max = math.max

dofile("_testing.scar")

import("btbcommon/btb_utils.scar")
import("btbcommon/btb_astar.scar")
import("btbcommon/btb_raycast.scar")

BTBAStar_OnInit()

local offset_x = 50.0
local offset_y = 50.0

local screen_width = 1000.0
local screen_height = 1000.0

love.window.setMode(screen_width, screen_height)

local size  = 800.0

local grid_size = ceil(35 / 4)
---@type (number|false)[]
local grid = BTBUtils.PreallocateTable(grid_size^2, 0)

--[[local grid = {
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1,
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1,
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1,
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1,
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1,
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1,
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
}

local grid_size = sqrt(#grid)]]

local cell_size = size / grid_size

---@param x integer
---@param y integer
---@return integer
local function getGridIndex(x, y)
	return x + (y-1) * grid_size
end

local function getGridXY(x, y)
	return max(1, min(grid_size, 1 + floor(max(0.0, x-offset_x) / cell_size))), max(1, min(grid_size, 1 + floor(max(0.0, y-offset_y) / cell_size)))
end

local weights = BTBUtils.PreallocateTable(grid_size, BTBUtils.PreallocateTable(grid_size, 0.0))
local function on_grid_update()
	for x=1,grid_size do
		for y=1,grid_size do
			weights[x][y] = grid[getGridIndex(x, y)]
		end
	end
end

---@param x integer
---@param y integer
---@param red number
---@param green number
---@param blue number
---@param alpha? number
local function drawCell(x, y, red, green, blue, alpha)
	love.graphics.setColor(red, green, blue, alpha)
	love.graphics.rectangle("fill", offset_x + cell_size * (x-1), offset_y + cell_size * (y-1), cell_size, cell_size)
end

local mouse_lmb_down = false
local mouse_rmb_down = false

local pos_start_x, pos_start_y = 200.0, 200.0
local pos_end_x, pos_end_y = 800.0, 800.0

local function getMouseGridXY()
	return getGridXY(love.mouse.getX(), love.mouse.getY())
end

local mouse_x = 0.0
local mouse_y = 0.0

local function on_lmb_release()
end

local function on_rmb_release()
end

local lmb_drawing = false
local rmb_drawing = false

---@param mouse_delta_x number
---@param mouse_delta_y number
local function on_mouse_move(mouse_delta_x, mouse_delta_y)
	if love.mouse.isDown(3) then
		offset_x = offset_x + mouse_delta_x
		offset_y = offset_y + mouse_delta_y
	end
end

local weight_max = 0.75
local weight_draw_speed = 5.0

function love.update()
	local new_mouse_x = love.mouse.getX()
	local new_mouse_y = love.mouse.getY()
	local mouse_delta_x = new_mouse_x - mouse_x
	local mouse_delta_y = new_mouse_y - mouse_y
	mouse_x = new_mouse_x
	mouse_y = new_mouse_y
	if abs(mouse_delta_x) > 0.0 or abs(mouse_delta_y) > 0.0 then
		on_mouse_move(mouse_delta_x, mouse_delta_y)
	end

	if love.mouse.isDown(1) then
		mouse_lmb_down = true
		if not rmb_drawing then
			lmb_drawing = true
		end
	elseif mouse_lmb_down then
		mouse_lmb_down = false
		on_lmb_release()
		lmb_drawing = false
	end
	if love.mouse.isDown(2) then
		mouse_rmb_down = true
		if not lmb_drawing then
			rmb_drawing = true
		end
	elseif mouse_rmb_down then
		mouse_rmb_down = false
		on_rmb_release()
		rmb_drawing = false
	end

	if love.keyboard.isDown("lctrl", "rctrl") then
		pos_start_x, pos_start_y = love.mouse.getPosition()
	elseif love.keyboard.isDown("lalt", "ralt") then
		pos_end_x, pos_end_y = love.mouse.getPosition()
	elseif lmb_drawing or rmb_drawing then
		local x, y = getMouseGridXY()
		local i = getGridIndex(x, y)
		local dt = love.timer.getDelta()
		if lmb_drawing then
			if love.keyboard.isDown("lshift", "rshift") then
				grid[i] = false
			elseif grid[i] then
				grid[i] = min(weight_max, grid[i] + weight_draw_speed * dt)
			end
		else
			if love.keyboard.isDown("lshift", "rshift") then
				grid[i] = 0.0
			elseif grid[i] then
				grid[i] = max(-weight_max, grid[i] - weight_draw_speed * dt)
			end
		end
		on_grid_update()
	end
end

local debug_font = love.graphics.newFont(12)
local debug_font_height = debug_font:getHeight()
---@param text string
---@param x? number
---@param y? number
local function debug_text(text, x, y)
	love.graphics.setFont(debug_font)
	love.graphics.setColor(0.2, 0.2, 0.2, 0.5)
	love.graphics.rectangle("fill", x or 0.0, y or 0.0, debug_font:getWidth(text)+10.0, debug_font_height+6.0)
	love.graphics.setColor(1.0, 1.0, 1.0)
	love.graphics.print(text, (x or 0.0) + 5.0, (y or 0.0) + 3.0)
end

---@param x integer
---@param y integer
---@return number
---@return number
local function cell_pos(x, y)
	return offset_x + x*cell_size - cell_size*0.5, offset_y + y*cell_size - cell_size*0.5
end

local function lineWithDots(x_1, y_1, x_2, y_2, r_1, g_1, b_1, a_1, r_2, g_2, b_2, a_2)
	love.graphics.setColor(r_1, g_1, b_1, a_1)
	love.graphics.line(x_1, y_1, x_2, y_2)
	local direction_x = x_2 - x_1
	local direction_y = y_2 - y_1
	local directionLen = sqrt(direction_x^2 + direction_y^2)
	local directionLenDiv = 1.0 / max(0.001, directionLen)
	direction_x = direction_x * directionLenDiv
	direction_y = direction_y * directionLenDiv
	love.graphics.setColor(r_2, g_2, b_2, a_2)
	local step_dist = 4.0
	local steps = floor(directionLen / step_dist)
	local step_x = direction_x * step_dist
	local step_y = direction_y * step_dist
	local pos_x = x_1 + step_x * 0.5
	local pos_y = y_1 + step_y * 0.5
	for _=0,steps-1 do
		pos_x = pos_x + step_x
		pos_y = pos_y + step_y
		love.graphics.circle("fill", pos_x, pos_y, 1.0)
	end
end

function love.draw()
	local draw_total = 0.0

	local perfstat_start

	perfstat_start = os.clock()
	for x=1,grid_size do
		for y=1,grid_size do
			local v = grid[getGridIndex(x, y)]
			if v == false then
				drawCell(x, y, 1.0, 1.0, 1.0, 0.5)
			elseif v > 0.0 then
				drawCell(x, y, 0.0, 1.0, 0.0, v/weight_max)
			else
				drawCell(x, y, 1.0, 0.0, 0.0, -v/weight_max)
			end
		end
	end

	love.graphics.setColor(0.25, 0.25, 0.25, 0.45)
	for x=0,grid_size do
		love.graphics.line(offset_x + x*cell_size, offset_y, offset_x + x*cell_size, offset_y + size)
	end
	for y=0,grid_size do
		love.graphics.line(offset_x, offset_y + y*cell_size, offset_x + size, offset_y + y*cell_size)
	end

	love.graphics.setColor(0.0, 1.0, 0.0)
	love.graphics.circle("line", pos_start_x, pos_start_y, 3.0)

	love.graphics.setColor(1.0, 1.0, 1.0, 0.15)
	love.graphics.line(pos_start_x, pos_start_y, pos_end_x, pos_end_y)
	draw_total = draw_total + os.clock() - perfstat_start

	local start_grid_x, start_grid_y = getGridXY(pos_start_x, pos_start_y)
	local end_grid_x, end_grid_y = getGridXY(pos_end_x, pos_end_y)
	perfstat_start = os.clock()
	local reachedDestination, t_x, t_y, n = BTB_Pathfind(grid_size, start_grid_x, start_grid_y, end_grid_x, end_grid_y, weights, grid_size^2, 2.0)
	debug_text(string.format("pathfind: %.0fms", (os.clock() - perfstat_start) * 1000.0), 0, (debug_font_height+6.0)*2)

	perfstat_start = os.clock()
	if t_x and t_y then
		--local r_1, g_1, b_1, r_2, g_2, b_2
		if reachedDestination then
			--r_1, g_1, b_1, r_2, g_2, b_2 = 0.0, 1.0, 0.0, 0.0, 0.0, 0.0
			love.graphics.setColor(0.0, 1.0, 0.0)
		else
			--r_1, g_1, b_1, r_2, g_2, b_2 = 0.0, 0.25, 0.75, 0.0, 0.0, 0.0
			love.graphics.setColor(0.0, 0.25, 0.75)
		end
		for i=1,n-1 do
			local pos1_x, pos1_y = cell_pos(t_x[i], t_y[i])
			local pos2_x, pos2_y = cell_pos(t_x[i+1], t_y[i+1])
			--lineWithDots(pos1_x, pos1_y, pos2_x, pos2_y, r_1, g_1, b_1, 1.0, r_2, g_2, b_2, 1.0)
			love.graphics.line(pos1_x, pos1_y, pos2_x, pos2_y)
		end
	end
	draw_total = draw_total + os.clock() - perfstat_start

	perfstat_start = os.clock()
	local hit_x, hit_y = BTB_Raycast(weights, offset_x, offset_y, size, pos_start_x, pos_start_y, pos_end_x, pos_end_y)
	debug_text(string.format("raycast: %.0fms", (os.clock() - perfstat_start) * 1000.0), 0, (debug_font_height+6.0)*3)

	perfstat_start = os.clock()
	if hit_x and hit_y then
		love.graphics.setColor(1.0, 0.0, 0.0, 0.75)
		love.graphics.line(pos_start_x, pos_start_y, hit_x, hit_y)
		love.graphics.circle("fill", hit_x, hit_y, 3.0)
		love.graphics.setColor(1.0, 0.0, 0.0)
	else
		love.graphics.setColor(0.0, 0.0, 1.0)
	end
	love.graphics.circle("line", pos_end_x, pos_end_y, 3.0)
	draw_total = draw_total + os.clock() - perfstat_start

	debug_text(string.format("draw: %.0fms", draw_total * 1000.0), 0, (debug_font_height+6.0)*1)

	if os.getenv("LOCAL_LUA_DEBUGGER_VSCODE") == "1" then
		debug_text(string.format("%.0f", love.timer.getFPS()))
	end
end
