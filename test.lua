
local existsDir = 'scar/'
local importDir = 'assets/scar/'

math.randomseed(math.floor(os.clock()))

--local run_start = os.time()

function FExists(name)
	local f=io.open(name,"r")
	if f~=nil then io.close(f) return true else return false end
end

---@diagnostic disable-next-line: lowercase-global
function import(file)
	if FExists(importDir..file) then
		dofile(importDir..file)
	elseif not FExists(existsDir..file) then
		error("File not found "..file)
	end
end

function World_GetWidth()
	return 550.0
end
function World_GetLength()
	return 550.0
end

function Loc_ToAnsi(loc)
	return tostring(loc)
end

function BP_GetName(bp)
	return bp
end

PBGTYPE_Squad = 1
PBGTYPE_Entity = 2
PBGTYPE_Ability = 3
PBGTYPE_Upgrade = 4
PBGTYPE_Race = 5

function BP_GetSquadBlueprint(name)
	return {PBGType_Squad, name}
end
function BP_GetEntityBlueprint(name)
	return {PBGType_Entity, name}
end
function BP_GetAbilityBlueprint(name)
	return {PBGType_Ability, name}
end
function BP_GetUpgradeBlueprint(name)
	return {PBGType_Upgrade, name}
end
function BP_GetRaceBlueprint(name)
	return {PBGType_Race, name}
end
function BP_GetType(bp)
	return bp[1]
end

function Player_GetResource()
	return 1000
end
function Player_IsHuman()
	return false
end
function AI_IsAIPlayer()
	return true
end
function AI_IsEnabled()
	return true
end
function Player_GetDisplayName(player)
	return "1312"
end
function Player_GetRace()
	return ""
end
function BP_GetEntityBlueprintsWithType_Internal()
	return {}
end
function Squad_HasVehicle(squad)
	return squad.isVehicle
end
function Squad_HasTeamWeapon(squad)
	return squad.hasTeamWeapon
end
function Squad_HasSetupTeamWeapon(squad)
	return squad.id % 2 == 0
end
function SyncWeapon_GetFromSGroup(sg)
	return sg[1].entities[1]
end
function SyncWeapon_IsAttacking()
	return false
end
function Squad_GetMax(squad)
	return #squad.entities
end
function Squad_Count(squad)
	return Squad_GetMax(squad)
end
function Squad_GetHealthPercentage(squad)
	return 0.1 + (squad.id % 3) * 0.8
end
function Squad_GetHealth()
	return 500
end
function Squad_GetHealthMax()
	return 600
end
function Squad_GetVeterancyRank(squad) return squad.id % 4 end
function Squad_GetAttackTargets() end
function Player_CanSeeSquad() return true end
function Player_HasUpgrade() return false end

function Game_FadeToBlack() end
function Setup_GetWinConditionOptions() end
function Player_SetResource() end
function Modify_PlayerResourceRate() end
function Modifier_ApplyToPlayer() end
function Camera_SetDefaultDeclination() end
function Camera_SetDefaultZoomDist() end
function Camera_ResetToDefault() end
function AI_LockSquad() end
function AI_LockEntity() end
function AI_EnableAll() end
function Rule_AddPlayerEvent() end
function Rule_AddSquadEvent() end
function Rule_RemoveSquadEvent() end
function EventRule_AddEvent() end
function Player_SetSquadProductionAvailability() end
function Player_SetEntityProductionAvailability() end
function Player_SetPopCapOverride() end
function LocalCommand_SquadMovePos() end
function LocalCommand_SquadMovePosFacing() end
function LocalCommand_SquadEntityLoad() end
function LocalCommand_SquadPos() end
function Entity_SimHide() end
function Entity_SetInvulnerable() end
function Entity_Destroy() end
function Cmd_AttackMove() end
function Cmd_Retreat() end
function Cmd_Stop() end
function Cmd_Ability() end
function Modify_ReceivedAccuracy() end
function Modify_ReceivedSuppression() end
function Modify_ReceivedDamage() end
function Modify_WeaponCooldown() end
function Modify_WeaponAccuracy() end
function Modify_WeaponScatter() end
function Squad_GetLastAttackers() end
function UI_AddChild() end
function UI_CreateDataContext() end

function Squad_FindCover(_, pos) return pos end

function Modifier_Create() return {} end

RACE = {}
setmetatable(RACE, {__index = function(t, str) return BP_GetRaceBlueprint(str) end})

SBP = {
	COMMON = {},
	AFRIKA_KORPS = {},
	AMERICAN = {},
	BRITISH = {},
	BRITISH_AFRICA = {},
	GERMAN = {},
}
setmetatable(SBP.COMMON        , {__index = function(t, str) return BP_GetSquadBlueprint(str) end})
setmetatable(SBP.AFRIKA_KORPS  , {__index = function(t, str) return BP_GetSquadBlueprint(str) end})
setmetatable(SBP.AMERICAN      , {__index = function(t, str) return BP_GetSquadBlueprint(str) end})
setmetatable(SBP.BRITISH       , {__index = function(t, str) return BP_GetSquadBlueprint(str) end})
setmetatable(SBP.BRITISH_AFRICA, {__index = function(t, str) return BP_GetSquadBlueprint(str) end})
setmetatable(SBP.GERMAN        , {__index = function(t, str) return BP_GetSquadBlueprint(str) end})

EBP = {
	COMMON = {},
	AFRIKA_KORPS = {},
	AMERICAN = {},
	BRITISH = {},
	BRITISH_AFRICA = {},
	GERMAN = {},
}
setmetatable(EBP.COMMON        , {__index = function(t, str) return BP_GetEntityBlueprint(str) end})
setmetatable(EBP.AFRIKA_KORPS  , {__index = function(t, str) return BP_GetEntityBlueprint(str) end})
setmetatable(EBP.AMERICAN      , {__index = function(t, str) return BP_GetEntityBlueprint(str) end})
setmetatable(EBP.BRITISH       , {__index = function(t, str) return BP_GetEntityBlueprint(str) end})
setmetatable(EBP.BRITISH_AFRICA, {__index = function(t, str) return BP_GetEntityBlueprint(str) end})
setmetatable(EBP.GERMAN        , {__index = function(t, str) return BP_GetEntityBlueprint(str) end})

ABILITY = {
	COMMON = {},
	AFRIKA_KORPS = {},
	AMERICAN = {},
	BRITISH = {},
	BRITISH_AFRICA = {},
	GERMAN = {},
}
setmetatable(ABILITY.COMMON        , {__index = function(t, str) return BP_GetAbilityBlueprint(str) end})
setmetatable(ABILITY.AFRIKA_KORPS  , {__index = function(t, str) return BP_GetAbilityBlueprint(str) end})
setmetatable(ABILITY.AMERICAN      , {__index = function(t, str) return BP_GetAbilityBlueprint(str) end})
setmetatable(ABILITY.BRITISH       , {__index = function(t, str) return BP_GetAbilityBlueprint(str) end})
setmetatable(ABILITY.BRITISH_AFRICA, {__index = function(t, str) return BP_GetAbilityBlueprint(str) end})
setmetatable(ABILITY.GERMAN        , {__index = function(t, str) return BP_GetAbilityBlueprint(str) end})

function Entity_GetBlueprint(entity)
	return entity.bp
end
function Squad_GetBlueprint(squad)
	return squad.bp
end
function BP_GetName(bp)
	return bp
end
function Player_GetEntityBPCost()
	return {manpower = 40.0, munition = 0, fuel = 0}
end
function Player_GetSquadBPCost()
	return {manpower = 240.0, munition = 0, fuel = 0}
end
function Player_GetAbilityBPCost()
	return {manpower = 0, munition = 25, fuel = 0}
end
function Entity_GetHealthMax(entity)
	return 300
end
function Entity_GetHealth()
	return 500
end
function Entity_GetHealthPercentage(entity)
	return 0.1 + (entity.id % 3) * 0.8
end
function Entity_IsAlive()
	return true
end
function Entity_IsBuilding(entity)
	return entity.id % 3 == 0
end
function Entity_CanLoadSquad(entity)
	return Entity_IsBuilding(entity)
end
function Entity_IsPartOfSquad()
	return false
end
function Entity_GetMaxHoldSquadSlots()
	return 0
end
function Entity_IsHoldingAny()
	return false
end
function Squad_CanCaptureStrategicPoint()
	return true
end
function BP_GetEntityBPArmorInfo(bp)
	return {armor=10, armor_side=10, armor_rear=5}
end
function BP_UpgradeExists() return true end

function World_Pos(x, y, z)
	return {x = x or 0, y = y or 0, z = z or 0}
end
function World_GetHeightAt(x, y) return 0 end
function Util_ScarPos(xpos, zpos, ypos)
	if ypos == nil then
		ypos = World_GetHeightAt(xpos,zpos)
	end
	return World_Pos(xpos, ypos, zpos)
end

function World_GetNearestInteractablePoint(pos)
	return pos
end

function World_GetPlayerCount()
	return 4
end
function World_GetPlayerAt(i)
	return i
end
function World_GetPlayerIndex(player)
	return player
end
function World_GetRand(min, max)
	return math.random(min, max)
end
function World_PosInBounds()
	return true
end

function World_DistanceSquaredPointToPoint(p1, p2)
	return (p1.x - p2.x) ^ 2 + (p1.z - p2.z) ^ 2
end
function World_DistancePointToPoint(p1, p2)
	return math.sqrt(World_DistanceSquaredPointToPoint(p1, p2))
end

function Territory_GetAdjacentSectors()
	return {}
end

function VictoryPoint_GetTeam()
	return {tickets = 350}
end

-----------------------------

function Core_GetPlayerTeamsEntry(player)
	return {
		index = 1 + math.floor(player / 3)
	}
end

-----------------------------

ST_NIL = 0
ST_SQUAD = 1
ST_ENTITY = 2

---@diagnostic disable-next-line: lowercase-global
function scartype(v)
	if not v then
		return ST_NIL
	end
	return v.scartype
end

local sID = 49999
local eID = 49999

local function NextSID()
	sID = sID + 1
	return sID
end
local function NextEID()
	eID = eID + 1
	return eID
end

local function RandPos()
	local w = World_GetWidth()
	local l = World_GetLength()
	return Util_ScarPos(math.random(w) - w * 0.5, math.random(l) - l * 0.5)
end

local sgroups = {
	["playerSquads__1"] = {},
	["playerSquads__2"] = {},
	["playerSquads__3"] = {},
	["playerSquads__4"] = {},
}
local squads = {}

function Squad_IsValid(squadID)
	for i=1,#squads do
		if squads[i].id == squadID then
			return true
		end
	end
	return false
end
function Squad_EntityAt(squad, i)
	return squad.entities[i+1]
end
function Squad_GetCoverLevel()
	return 0.5
end
function Squad_IsRetreating()
	return false
end
function Squad_IsUnderAttack(squad)
	return squad.id % 3 ~= 0
end
function Squad_IsAttacking(squad)
	return squad.id % 2 == 0
end
function Squad_IsMoving(squad)
	return squad.id % 3 == 0
end
function Squad_IsPinned()
	return false
end
function Squad_IsSuppressed()
	return false
end
function Squad_HasUpgrade()
	return false
end
function Squad_IsInHoldSquad()
	return false
end
function Squad_IsInHoldEntity()
	return false
end
function Squad_HasDestination(squad)
	return Squad_IsMoving(squad)
end
function Squad_GetDestination()
	return Util_ScarPos(0, 0)
end
function Squad_GetHeading()
	return World_Pos(1, 0, 0)
end
function Squad_IsConstructing()
	return false
end
function Squad_CanInstantReinforceNow()
	return false
end

function BP_GetSquadBlueprintUpgrades()
	return {}
end

function SGroup_CreateIfNotFound(name)
	if not sgroups[name] then
		sgroups[name] = {}
	end
	return sgroups[name]
end
function SGroup_CreateUnique()
	local sg = {}
	sgroups[#sgroups+1] = sg
	return sg
end
function SGroup_CountSpawned(sg)
	return #sg
end
function SGroup_GetSpawnedSquadAt(sg, i)
	return sg[i]
end
function SGroup_Single(sg, squad)
	sg[1] = squad
	for i=2,#sg do
		sg[i] = nil
	end
end
function SGroup_Clear(sg)
	for i=1,#sg do
		sg[i] = nil
	end
end
function SGroup_Add(sg, squad)
	sg[#sg+1] = squad
end
function SGroup_AddGroup(sg, sgToAdd)
	for i=1,#sgToAdd do
		sg[#sg+1] = sgToAdd[i]
	end
end
function SGroup_Remove(sg, squad)
	for i=1,#sg do
		if sg[i] == squad then
			for j=i,#sg do
				sg[j] = sg[j+1]
			end
			break
		end
	end
end
function SGroup_RemoveGroup(sg, sg2)
	for i=1,#sg2 do
		SGroup_Remove(sg, sg2[i])
	end
end
function Player_GetSquads(player)
	return sgroups["playerSquads__"..player]
end
function Squad_FromID(squadID)
	for i=1,#squads do
		if squads[i].id == squadID then
			return squads[i]
		end
	end
end
function Squad_CreateToward(bp, p, loadoutCount, pos, dir)
	local squad = {id = NextSID(), scartype = ST_SQUAD, player = p, isVehicle = false, hasTeamWeapon = false, position = pos, bp = bp}
	squads[#squads+1] = squad
	table.insert(sgroups["playerSquads__"..p], squad)
end

local egroups = {}
local entities = {}

local w = World_GetWidth()
local l = World_GetLength()

local territory = {}
local territoryDivisions = 5

function Entity_IsValid(entityID)
	for i=1,#entities do
		if entities[i].id == entityID then
			return true
		end
	end
	return false
end
function Entity_CreateENV(bp, pos)
	return {id = -1, scartype = ST_ENTITY, player = 0, bp = bp, position = pos}
end
function Entity_SetPosition(entity, pos)
	entity.position = pos
end
function Entity_GetCoverValue() return 0.5 end
function World_GetTerritorySectorID(pos)
	return territory[math.min(territoryDivisions, math.floor((pos.x+w*0.5)/w/territoryDivisions) + 1)][math.min(territoryDivisions, math.floor((pos.z+l*0.5)/l/territoryDivisions) + 1)].id
end
function Territory_GetSectorCreatorEntity(territoryID)
	for i=1,#entities do
		if entities[i].id == territoryID then
			return entities[i]
		end
	end
	return nil
end
function World_GetStrategyPoints(eg)
	for x=1,#territory do
		for y=1,#territory[x] do
			eg[#eg+1] = territory[x][y]
		end
	end
end
function Entity_IsStrategicPoint(entity)
	local eg = EGroup_CreateIfNotFound('Entity_IsStrategicPoint')
	EGroup_Clear(eg)
	World_GetStrategyPoints(eg)
	for i=1,#eg do
		if eg[i] == entity then
			return true
		end
	end
	return false
end
function Entity_IsVictoryPoint(entity)
	local eg = EGroup_CreateIfNotFound('Entity_IsVictoryPoint')
	EGroup_Clear(eg)
	World_GetStrategyPoints(eg)
	for i=1,#eg do
		if eg[i] == entity then
			return i % 33 == 0
		end
	end
	return false
end
function Entity_IsSyncWeapon()
	return false
end
function Squad_IsOfType(squad, type)
	return type == 'vehicle' and squad.isVehicle or type == 'infantry' and not squad.isVehicle
end
function Entity_IsOfType(entity, type)
	return type == 'infantry'
end
function World_OwnsEntity(entity)
	return entity.player < 1
end

function EGroup_CreateIfNotFound(name)
	if not egroups[name] then
		egroups[name] = {}
	end
	return egroups[name]
end
function EGroup_CreateUnique()
	local eg = {}
	egroups[#egroups+1] = eg
	return eg
end
function EGroup_CountSpawned(eg)
	return #eg
end
function EGroup_GetSpawnedEntityAt(eg, i)
	return eg[i]
end
function EGroup_Single(eg, entity)
	eg[1] = entity
	for i=2,#eg do
		eg[i] = nil
	end
end
function EGroup_Clear(eg)
	for i=1,#eg do
		eg[i] = nil
	end
end
function EGroup_Add(eg, entity)
	eg[#eg+1] = entity
end
function EGroup_Destroy(eg)
	for i,v in pairs(egroups) do
		if v == eg then
			egroups[i] = nil
		end
	end
end
function Player_GetEntities(player)
	return egroups["playerEntities__"..player]
end
function Player_CanSeePosition()
	return true
end

local startingPositions = {
	Util_ScarPos( w*0.475,  l*0.475),
	Util_ScarPos(-w*0.475,  l*0.475),
	Util_ScarPos( w*0.475, -l*0.475),
	Util_ScarPos(-w*0.475, -l*0.475),
}
function Player_GetStartingPosition(player)
	return startingPositions[player]
end

------------------------------

function Squad_GetID(squad)
	return squad.id
end
function Entity_GetID(entity)
	return entity.id
end
function Squad_GetPlayerOwner(squad)
	return squad.player
end
function Squad_GetPosition(squad)
	return squad.position
end
function Entity_GetPosition(entity)
	return entity.position
end
function Entity_GetPlayerOwner(entity)
	return entity.player
end

------------------------------

local modules = {}
function Core_RegisterModule(name)
	modules[#modules+1] = name
end

---@diagnostic disable-next-line: lowercase-global
printCpy = print
print = function() end

import('winconditions/BackToBasics.scar')

function Util_GetRandomPosition(pos, maxDist)
	local dist = math.random(maxDist * 1000) / 1000
	local angle = math.random(36000) / 100
	return VTowardsAngle(pos, angle, dist)
end

OT_Ally = 1
OT_Enemy = 2
function World_GetSquadsNearPoint(player, sg, pos, radius, relationship)
	--[[local teamIndex = BTBUtils.TeamIndex(player)
	if relationship == OT_Enemy then
		teamIndex = BTBUtils.EnemyTeamIndex(teamIndex)
	end
	local result = AAI_D_QuerySquads(pos, teamIndex, radius)
	for i=1,#result do
		sg[#sg+1] = result[i]
	end]]
	local radiusSq = radius ^ 2
	local teamIndex = BTBUtils.TeamIndex(player)
	for i=1,#squads do
		local squad = squads[i]
		if relationship == OT_Player and squad.player == player or
				relationship == OT_Ally and BTBUtils.TeamIndex(squad.player) == teamIndex or
				relationship == OT_Enemy and BTBUtils.TeamIndex(squad.player) ~= teamIndex then
			local distSq = World_DistanceSquaredPointToPoint(squad.position, pos)
			if distSq < radiusSq then
				sg[#sg+1] = squad
			end
		end
	end
end
function World_GetEntitiesNearPoint(player, eg, pos, radius, relationship)
	--[[local teamIndex = BTBUtils.TeamIndex(player)
	if relationship == OT_Enemy then
		teamIndex = BTBUtils.EnemyTeamIndex(teamIndex)
	end
	local result = AAI_D_QueryEntities(pos, teamIndex, radius)
	for i=1,#result do
		eg[#eg+1] = result[i]
	end]]
	local radiusSq = radius ^ 2
	local teamIndex = BTBUtils.TeamIndex(player)
	for i=1,#entities do
		local entity = entities[i]
		if relationship == OT_Player and entity.player == player or
				relationship == OT_Ally and BTBUtils.TeamIndex(entity.player) == teamIndex or
				relationship == OT_Enemy and BTBUtils.TeamIndex(entity.player) ~= teamIndex then
			local distSq = World_DistanceSquaredPointToPoint(entity.position, pos)
			if distSq < radiusSq then
				eg[#eg+1] = entity
			end
		end
	end
end
function World_GetNeutralEntitiesNearPoint(eg, pos, radius)
	local radiusSq = radius ^ 2
	for i=1,#entities do
		local entity = entities[i]
		if entity.player < 1 then
			local distSq = World_DistanceSquaredPointToPoint(entity.position, pos)
			if distSq < radiusSq then
				eg[#eg+1] = squad
			end
		end
	end
end

local rules = {}
function Rule_Add(fn)
	rules[#rules+1] = {fn, 0.125}
end
function Rule_AddInterval(fn, interval)
	rules[#rules+1] = {fn, math.floor(interval / 0.125) * 0.125}
end

------------------------------

local time = 0.0
function World_GetGameTime()
	return time --math.floor(os.time() - run_start / 0.125) * 0.125
end

------------------------------

--collectgarbage('stop')

local benchStart
local timeDiff
local avgMS
local longestRunDuration

-----------------------------

--[[local basicTestRepeats = 50000

local t1 = BTBUtils.PreallocateTable(20)
for i=1,#t1 do
	t1[i] = i
end
local t2 = {
	[1] = 1,
	[2] = 2,
	[3] = 3,
	[4] = 4,
	[5] = 5,
	[6] = 6,
	[7] = 7,
	[8] = 8,
	[9] = 9,
	[10] = 10,
}

benchStart = os.clock()
longestRunDuration = 0.0

local a

a = 0
for _=1,basicTestRepeats do
	local curRepeatStart = os.clock()

	for i=1,1000 do
		a = a + t2[i%10+1]
	end

	local curRunDuration = os.clock() - curRepeatStart
	if curRunDuration > longestRunDuration then
		longestRunDuration = curRunDuration
	end
end
timeDiff = os.clock() - benchStart
avgMS = timeDiff/basicTestRepeats*1000
printCpy(string.format("Basic test 2: A: %d, %5.3fs (%5.3fms avg) Longest: %2dms", a, timeDiff, avgMS, math.floor(longestRunDuration*1000)))

a = 0
for _=1,basicTestRepeats do
	local curRepeatStart = os.clock()

	for i=1,1000 do
		a = a + t1[i%10+1]
	end

	local curRunDuration = os.clock() - curRepeatStart
	if curRunDuration > longestRunDuration then
		longestRunDuration = curRunDuration
	end
end
timeDiff = os.clock() - benchStart
avgMS = timeDiff/basicTestRepeats*1000
printCpy(string.format("Basic test 1: A: %d, %5.3fs (%5.3fms avg) Longest: %2dms", a, timeDiff, avgMS, math.floor(longestRunDuration*1000)))]]

----------------------

local baseMS = 15

local passes = 10
local repeats = 150
local fnRepeats = 50
local fnTests = {
	'AAI_C_Update_Secondary',
	'AAI_D_Update_Entity',
	'AAI_D_Update_InitStep_1',
	'AAI_D_Update_InitStep_2',
	'AAI_D_Update_InitStep_3',
}

printCpy(string.format("Primary passes: %d repeats: %d fnRepeats: %d expected base tick FPS: %1.f", passes, repeats, fnRepeats, 1000 / baseMS))
printCpy("Running bench...")
for pass=1,passes do
	rules = {}

	sID = 49999
	eID = 49999
	squads = {}
	sgroups = {}

	--local numInfantry = 14
	--local numVehicles = 9
	--local numHMGs = 6
	--local numMortars = 4

	local numInfantry = 2
	local numVehicles = 2
	local numHMGs = 2
	local numMortars = 2

	for p=1,4 do
		sgroups["playerSquads__"..p] = {}
		for _=1,numInfantry do
			local squad = {id = NextSID(), scartype = ST_SQUAD, player = p, isVehicle = false, hasTeamWeapon = false, position = RandPos(), bp = SBP.AMERICAN.RIFLEMEN_US}
			squads[#squads+1] = squad
			table.insert(sgroups["playerSquads__"..p], squad)
		end
		for _=1,numVehicles do
			local squad = {id = NextSID(), scartype = ST_SQUAD, player = p, isVehicle = true , hasTeamWeapon = false, position = RandPos(), bp = SBP.AMERICAN.SHERMAN_US}
			squads[#squads+1] = squad
			table.insert(sgroups["playerSquads__"..p], squad)
		end
		for _=1,numHMGs do
			local squad = {id = NextSID(), scartype = ST_SQUAD, player = p, isVehicle = false, hasTeamWeapon = true , position = RandPos(), bp = SBP.AMERICAN.HMG_30CAL_US}
			squads[#squads+1] = squad
			table.insert(sgroups["playerSquads__"..p], squad)
		end
		for _=1,numMortars do
			local squad = {id = NextSID(), scartype = ST_SQUAD, player = p, isVehicle = false, hasTeamWeapon = true , position = RandPos(), bp = SBP.AMERICAN.MORTAR_81MM_US}
			squads[#squads+1] = squad
			table.insert(sgroups["playerSquads__"..p], squad)
		end
	end

	for i=1,#squads do
		local squad = squads[i]
		if Squad_HasVehicle(squad) then
			squad.entities = {
				{id = NextEID(), player = squad.player, bp = EBP.AMERICAN.SHERMAN_US, position = squad.position}
			}
		else
			squad.entities = {}
			for j=1,6 do
				squad.entities[j] = {id = NextEID(), player = squad.player, bp = EBP.AMERICAN.RIFLEMAN_US, position = squad.position}
			end
		end
	end

	entities = {}
	egroups = {}

	local numEntitiesPerPlayer = 7
	for p=1,4 do
		egroups["playerEntities__"..p] = {}
		for _=1,numEntitiesPerPlayer do
			local entity = {id = NextEID(), scartype = ST_ENTITY, player = p, bp = EBP.AMERICAN.EMPLACEMENT_MACHINE_GUN_US, position = RandPos()}
			entities[#entities+1] = entity
			table.insert(egroups["playerEntities__"..p], entity)
		end
	end

	territory = {}
	for x=1,territoryDivisions do
		territory[x] = {}
		for y=1,territoryDivisions do
			local entity = {id = NextEID(), player = 0, bp = BP_GetEntityBlueprint('territory_fuel_point_low'),
				position = Util_ScarPos(w / territoryDivisions * x - w * 0.5, l / territoryDivisions * y - l * 0.5)}
			territory[x][y] = entity
			entities[#entities+1] = entity
		end
	end

	time = 0.0
	for i=1,#modules do
		local init = _G[modules[i].."_OnInit"]
		if init then
			init()
		end
	end
	time = time + 0.125
	for i=1,#modules do
		local start = _G[modules[i].."_Start"]
		if start then
			start()
		end
	end

	--collectgarbage('collect')

	longestRunDuration = 0.0
	benchStart = os.clock()
	for _=1,repeats do
		local curRepeatStart = os.clock()
		AAI_Scheduler()
		--BTBCombat_Tick()
		time = time + 0.125
		local curRunDuration = os.clock() - curRepeatStart
		if curRunDuration > longestRunDuration then
			longestRunDuration = curRunDuration
		end
		--printCpy(string.format("  run: %2dms", math.floor(curRunDuration*1000)))
	end
	timeDiff = os.clock() - benchStart
	avgMS = timeDiff/repeats*1000
	printCpy(string.format("Primary pass %2d, %5.3fs (%5.3fms avg) Longest: %2dms FPS: %.1f / %.1f Mem: %.1f",
		pass, timeDiff, avgMS, math.floor(longestRunDuration*1000), 1000 / (baseMS + avgMS), 1000 / (baseMS + longestRunDuration*1000), collectgarbage('count')))

	--[[for i=1,#fnTests do
		local fn = _G[ fnTests[i] ]
		longestRunDuration = 0.0
		benchStart = os.clock()
		for _=1,fnRepeats do
			local curRepeatStart = os.clock()
			fn()
			local curRunDuration = os.clock() - curRepeatStart
			if curRunDuration > longestRunDuration then
				longestRunDuration = curRunDuration
			end
		end
		timeDiff = os.clock() - benchStart
		avgMS = timeDiff/fnRepeats*1000
		printCpy(string.format("  %23s: %5.3fs (%5.3fms avg) Longest: %dms FPS: %.1f / %.1f", fnTests[i],
			timeDiff, avgMS, math.floor(longestRunDuration*1000), 1000 / (baseMS + avgMS), 1000 / (baseMS + longestRunDuration*1000)))
	end]]
end

os.exit()
